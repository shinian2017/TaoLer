<?php return array(
    'root' => array(
        'pretty_version' => 'dev-master',
        'version' => 'dev-master',
        'type' => 'project',
        'install_path' => __DIR__ . '/../../',
        'aliases' => array(),
        'reference' => '1250b449335a867caa1976e6117d3b3448b5d7bf',
        'name' => 'taoser/taoler',
        'dev' => true,
    ),
    'versions' => array(
        'firebase/php-jwt' => array(
            'pretty_version' => 'v5.5.1',
            'version' => '5.5.1.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../firebase/php-jwt',
            'aliases' => array(),
            'reference' => '83b609028194aa042ea33b5af2d41a7427de80e6',
            'dev_requirement' => false,
        ),
        'league/flysystem' => array(
            'pretty_version' => '1.1.9',
            'version' => '1.1.9.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../league/flysystem',
            'aliases' => array(),
            'reference' => '094defdb4a7001845300334e7c1ee2335925ef99',
            'dev_requirement' => false,
        ),
        'league/flysystem-cached-adapter' => array(
            'pretty_version' => '1.1.0',
            'version' => '1.1.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../league/flysystem-cached-adapter',
            'aliases' => array(),
            'reference' => 'd1925efb2207ac4be3ad0c40b8277175f99ffaff',
            'dev_requirement' => false,
        ),
        'league/mime-type-detection' => array(
            'pretty_version' => '1.11.0',
            'version' => '1.11.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../league/mime-type-detection',
            'aliases' => array(),
            'reference' => 'ff6248ea87a9f116e78edd6002e39e5128a0d4dd',
            'dev_requirement' => false,
        ),
        'liliuwei/thinkphp-social' => array(
            'pretty_version' => 'v1.3',
            'version' => '1.3.0.0',
            'type' => 'think-extend',
            'install_path' => __DIR__ . '/../liliuwei/thinkphp-social',
            'aliases' => array(),
            'reference' => '2067fc2c2cc3b3d109602bc19c3e5a99c5f4c970',
            'dev_requirement' => false,
        ),
        'lotofbadcode/phpspirit_databackup' => array(
            'pretty_version' => 'v1.1',
            'version' => '1.1.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../lotofbadcode/phpspirit_databackup',
            'aliases' => array(),
            'reference' => '2627b2e4206031731c94c8d637fb06b3b96e8860',
            'dev_requirement' => false,
        ),
        'phpmailer/phpmailer' => array(
            'pretty_version' => 'v6.6.0',
            'version' => '6.6.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../phpmailer/phpmailer',
            'aliases' => array(),
            'reference' => 'e43bac82edc26ca04b36143a48bde1c051cfd5b1',
            'dev_requirement' => false,
        ),
        'psr/cache' => array(
            'pretty_version' => '1.0.1',
            'version' => '1.0.1.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../psr/cache',
            'aliases' => array(),
            'reference' => 'd11b50ad223250cf17b86e38383413f5a6764bf8',
            'dev_requirement' => false,
        ),
        'psr/container' => array(
            'pretty_version' => '1.1.2',
            'version' => '1.1.2.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../psr/container',
            'aliases' => array(),
            'reference' => '513e0666f7216c7459170d56df27dfcefe1689ea',
            'dev_requirement' => false,
        ),
        'psr/http-message' => array(
            'pretty_version' => '1.0.1',
            'version' => '1.0.1.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../psr/http-message',
            'aliases' => array(),
            'reference' => 'f6561bf28d520154e4b0ec72be95418abe6d9363',
            'dev_requirement' => false,
        ),
        'psr/log' => array(
            'pretty_version' => '1.1.4',
            'version' => '1.1.4.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../psr/log',
            'aliases' => array(),
            'reference' => 'd49695b909c3b7628b6289db5479a1c204601f11',
            'dev_requirement' => false,
        ),
        'psr/simple-cache' => array(
            'pretty_version' => '1.0.1',
            'version' => '1.0.1.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../psr/simple-cache',
            'aliases' => array(),
            'reference' => '408d5eafb83c57f6365a3ca330ff23aa4a5fa39b',
            'dev_requirement' => false,
        ),
        'symfony/polyfill-mbstring' => array(
            'pretty_version' => 'v1.25.0',
            'version' => '1.25.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/polyfill-mbstring',
            'aliases' => array(),
            'reference' => '0abb51d2f102e00a4eefcf46ba7fec406d245825',
            'dev_requirement' => true,
        ),
        'symfony/polyfill-php72' => array(
            'pretty_version' => 'v1.25.0',
            'version' => '1.25.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/polyfill-php72',
            'aliases' => array(),
            'reference' => '9a142215a36a3888e30d0a9eeea9766764e96976',
            'dev_requirement' => true,
        ),
        'symfony/polyfill-php80' => array(
            'pretty_version' => 'v1.25.0',
            'version' => '1.25.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/polyfill-php80',
            'aliases' => array(),
            'reference' => '4407588e0d3f1f52efb65fbe92babe41f37fe50c',
            'dev_requirement' => true,
        ),
        'symfony/var-dumper' => array(
            'pretty_version' => 'v4.4.39',
            'version' => '4.4.39.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/var-dumper',
            'aliases' => array(),
            'reference' => '35237c5e5dcb6593a46a860ba5b29c1d4683d80e',
            'dev_requirement' => true,
        ),
        'taoser/taoler' => array(
            'pretty_version' => 'dev-master',
            'version' => 'dev-master',
            'type' => 'project',
            'install_path' => __DIR__ . '/../../',
            'aliases' => array(),
            'reference' => '1250b449335a867caa1976e6117d3b3448b5d7bf',
            'dev_requirement' => false,
        ),
        'taoser/think-addons' => array(
            'pretty_version' => 'v1.0.1',
            'version' => '1.0.1.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../taoser/think-addons',
            'aliases' => array(),
            'reference' => 'bd8b0bfa4543fe8d2da65355c134250f78c0d457',
            'dev_requirement' => false,
        ),
        'taoser/think-auth' => array(
            'pretty_version' => 'v1.0.0',
            'version' => '1.0.0.0',
            'type' => 'think-extend',
            'install_path' => __DIR__ . '/../taoser/think-auth',
            'aliases' => array(),
            'reference' => '19bb04e4fb957a95ff3fdc142939922c19167b43',
            'dev_requirement' => false,
        ),
        'taoser/think-setarr' => array(
            'pretty_version' => 'v0.0.3',
            'version' => '0.0.3.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../taoser/think-setarr',
            'aliases' => array(),
            'reference' => '6651c31ef42417a6294ef08e6fb970917b7e7f86',
            'dev_requirement' => false,
        ),
        'topthink/framework' => array(
            'pretty_version' => 'v6.0.12',
            'version' => '6.0.12.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../topthink/framework',
            'aliases' => array(),
            'reference' => 'e478316ac843c1a884a3b3a7a94db17c4001ff5c',
            'dev_requirement' => false,
        ),
        'topthink/think-captcha' => array(
            'pretty_version' => 'v3.0.6',
            'version' => '3.0.6.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../topthink/think-captcha',
            'aliases' => array(),
            'reference' => '3b60feb4d484381ff7ddda4af7582a6b9be5dacf',
            'dev_requirement' => false,
        ),
        'topthink/think-helper' => array(
            'pretty_version' => 'v3.1.6',
            'version' => '3.1.6.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../topthink/think-helper',
            'aliases' => array(),
            'reference' => '769acbe50a4274327162f9c68ec2e89a38eb2aff',
            'dev_requirement' => false,
        ),
        'topthink/think-installer' => array(
            'pretty_version' => 'v2.0.5',
            'version' => '2.0.5.0',
            'type' => 'composer-plugin',
            'install_path' => __DIR__ . '/../topthink/think-installer',
            'aliases' => array(),
            'reference' => '38ba647706e35d6704b5d370c06f8a160b635f88',
            'dev_requirement' => false,
        ),
        'topthink/think-migration' => array(
            'pretty_version' => 'v3.0.3',
            'version' => '3.0.3.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../topthink/think-migration',
            'aliases' => array(),
            'reference' => '5717d9e5f3ea745f6dbfd1e30b4402aaadff9a79',
            'dev_requirement' => false,
        ),
        'topthink/think-multi-app' => array(
            'pretty_version' => 'v1.0.14',
            'version' => '1.0.14.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../topthink/think-multi-app',
            'aliases' => array(),
            'reference' => 'ccaad7c2d33f42cb1cc2a78d6610aaec02cea4c3',
            'dev_requirement' => false,
        ),
        'topthink/think-orm' => array(
            'pretty_version' => 'v2.0.53',
            'version' => '2.0.53.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../topthink/think-orm',
            'aliases' => array(),
            'reference' => '06783eda65547a70ea686360a897759e1f873fff',
            'dev_requirement' => false,
        ),
        'topthink/think-template' => array(
            'pretty_version' => 'v2.0.8',
            'version' => '2.0.8.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../topthink/think-template',
            'aliases' => array(),
            'reference' => 'abfc293f74f9ef5127b5c416310a01fe42e59368',
            'dev_requirement' => false,
        ),
        'topthink/think-trace' => array(
            'pretty_version' => 'v1.4',
            'version' => '1.4.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../topthink/think-trace',
            'aliases' => array(),
            'reference' => '9a9fa8f767b6c66c5a133ad21ca1bc96ad329444',
            'dev_requirement' => true,
        ),
        'topthink/think-view' => array(
            'pretty_version' => 'v1.0.14',
            'version' => '1.0.14.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../topthink/think-view',
            'aliases' => array(),
            'reference' => 'edce0ae2c9551ab65f9e94a222604b0dead3576d',
            'dev_requirement' => false,
        ),
        'wamkj/thinkphp6.0-databackup' => array(
            'pretty_version' => 'v1.0',
            'version' => '1.0.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../wamkj/thinkphp6.0-databackup',
            'aliases' => array(),
            'reference' => '28a0e406d827132942723a3c9f69bb20c98e652f',
            'dev_requirement' => false,
        ),
    ),
);
